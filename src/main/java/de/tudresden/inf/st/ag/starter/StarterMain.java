package de.tudresden.inf.st.ag.starter;
import org.jastadd.ag.ast.*;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.nio.charset.StandardCharsets;

public class StarterMain {
  public static void main(String[] args){
	  System.out.println("Hello, please input amount of disks:");
	  int amountOfDisk = 0;
      Scanner sc=new Scanner(System.in);
	    while(amountOfDisk <= 0){//input #disks
	      String s=sc.next();
	      amountOfDisk = checkInt(s); 
	    }
	    sc.close();
	    game(amountOfDisk);
    }
  public static void game(int i) {	    
	    Hanoi hanoi;
	    hanoi = new Hanoi();
	    hanoi.Initialisation(i);
	    hanoi.play();
	    System.out.println("Game done.");
	    hanoi.printResult();
  }
  public static int checkInt(String str){
    int a =0;
    try {
      a = Integer.parseInt(str);
      } catch (NumberFormatException e) {
        return 0;
      }
    return a;
  }  
}