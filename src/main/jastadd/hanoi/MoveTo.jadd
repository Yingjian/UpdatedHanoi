aspect MoveTo {
  public int Constraint.check(Pillar P0, Pillar P1, boolean turn){
    if(turn==true){
      if(this.oddTurn(P0, P1)){
        return 1;
      }
    }
    if(this.evenTurn(P0, P1)){
      return 2;
    }
    return 0;
  }
  /*move the disk from this to P, eg. P0.moveTo(P2).
                              Hanoi                                        Hanoi
                                |                                            |
                ----------------------------------           ----------------------------------
                |               |                |           |               |                |
               P0               P1               P2         P0               P1               P2
                |                                            |                                |
-----------------------                           -----------------------                     D0
|     |    ......     |                           |     |    ......     |
Dn-1  Dn-2            D0                          Dn-1  Dn-2            D1
  */
  public boolean Pillar.moveTo(Pillar P){
      int i = this.getNumDisk();
      P.addDisk(this.getDisk(i-1));
      this.getDisks().removeChild(i-1);
      return true;
  }
  public boolean Constraint.oddTurn(Pillar P0, Pillar P1){
    /*for every odd turn(turn==true), move the smallest disk D0 in sequence.
    Therefore, CS checks for a move from P0 to P1, when D0 is on the top of P0.
    Conjunction()-------------SD()---------R0()---------T0()
                          |                  |----------T1()
                          |---CS()---------R1()---------T2()
                                                  |
                                                  |-----T3()*/
          Conjunction C=new Conjunction();
          Atom SD=new Atom();//if the smallest disk is on this pillar
          Atom CS=new Atom();//check if the move sequence from P0 to P1 is valid
          Equal R0= new Equal();
          Equal R1= new Equal();
          Term1 T0=new Term1();//return size of top disk, return -1 when no disk
          ConstantNum T1=new ConstantNum();
          Term2 T2=new Term2();//return the target pillar's ID for this pillar in odd turns
          Term3 T3=new Term3();//return the ID for this pillar
          T0.setRel(P0);
          T1.setNum(1);
          T2.setRel(P0);
          T3.setRel(P1);
          R0.setLeft(T0);
          R0.setRight(T1);
          R1.setLeft(T2);
          R1.setRight(T3);
    
          SD.setRelation(R0);
          CS.setRelation(R1);
          C.setLeft(SD);
          C.setRight(CS);
          this.addConnective(C);
          if(this.getConnective(0).eval()){
            System.out.println("Odd turn: P" + P0.ID() +".hasD0 && (P" + P0.ID() +".target == P" + P1.ID()+")");
            return true;//odd turn and valid move
          }else{
            return false;
          }
        }
        public boolean Constraint.evenTurn(Pillar P0, Pillar P1){
      /*for every even turn(turn==false), move the disk D on top of P0 to P1:
      D is not D0(the smallest disk).
      There is always one D can be moved in the even turn according to the game rules.
      Statements:
      1.P0 is not empty: not OND().
      2.When condition 1 holds, P1 can be empty: TND().
      3.When 1 holds and 2 doesn’t hold, D0 < D1(for the disks on the top of P0 and P1 respectively).
      Therefore, we want not OND() and (TND() or CS()) to be true.
                                                     |-----------SD()-----------------R0---------------T0()
                                  C1                 |C3                               |----------------T3()
      Conjunction()-------------Negation()------Disjunction()----OND()----------------R1()----------------T0()
          C0                |                                                            |----------------T2()
                            |---Disjunction()-----------TND()--------------R2()---------T1()
                                    |C2                                       |-----------T2()
                                    |------------------CS()-----------------R3()---------T0()
                                                                              |---------T1*/
          Conjunction C0=new Conjunction();
          Negation C1=new Negation();
          Disjunction C2=new Disjunction();
          Disjunction C3=new Disjunction();
          Atom SD=new Atom();//if the smallest disk is on this pillar
          Atom OND=new Atom();//origin has no disk
          Atom TND=new Atom();//target has no disk
          Atom CS=new Atom();//check the size, if the disk on the top of origin has smaller size than the one on target
      
          C0.setLeft(C1);
          C0.setRight(C2);
          C1.setConnective(C3);
          C2.setLeft(TND);
          C2.setRight(CS);
          C3.setLeft(SD);
          C3.setRight(OND);
      
          Term1 T0=new Term1();//return size of top disk, return -1 when no disk.
          Term1 T1=new Term1();
          ConstantNum T2=new ConstantNum();
          ConstantNum T3=new ConstantNum();
      
          T0.setRel(P0);
          T1.setRel(P1);
          T2.setNum(0);
          T3.setNum(1);
      
          Equal R0=new Equal();
          Compare R1=new Compare();
          Compare R2=new Compare();
          Compare R3=new Compare();
      
          R0.setLeft(T0);
          R0.setRight(T3);
          R1.setLeft(T0);
          R1.setRight(T2);
          R2.setLeft(T1);
          R2.setRight(T2);
          R3.setLeft(T0);
          R3.setRight(T1);
          SD.setRelation(R0);
          OND.setRelation(R1);
          TND.setRelation(R2);
          CS.setRelation(R3);
          
          this.addConnective(C0);
          if(this.getConnective(0).eval()){
            System.out.println("Even turn: !(P" + P0.ID() +".#disk == 0 || P" + P0.ID() +".hasD0) && (P" + P1.ID() +".#disk == 0 || P" + P0.ID() +".topDisk < P" + P1.ID() +".topDisk)");
            return true;//even turn and valid move
          }
          return false;
        }
}
