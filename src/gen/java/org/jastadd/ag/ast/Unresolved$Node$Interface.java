package org.jastadd.ag.ast;

import java.util.*;

/**
 * @ast interface
 * @aspect RefResolverHelpers
 * @declaredat E:\\project\\20211201\\src\\gen\\jastadd\\agRefResolver.jadd:444
 */
 interface Unresolved$Node$Interface {

     
    String getUnresolved$Token();

     
    boolean getUnresolved$ResolveOpposite();
}
