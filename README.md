# Hanoi_JastAdd
- [ ] ./gradlew build
- [ ] ./gradlew jarAll
- [ ] java -jar Hanoi-all.jar
 
If we regard Atom as the smallest element, then we are under the circumstance of propositional logic, each Atom is a statement.
However, proposition like "the pillar has no disk" is less flexable and lack reusablity. Therefore we introduce the definition for atoms in first order logic.  
An atom stands for the relation of terms, of one term or no term(goes back to the definition of propositional logic).  
A term stands for the function of variables or a variable or no variable(constant).  
Therefore, we have binary relations:  
Compare return Left<right   
Equal return Left==right  
//////////////////////////////////////////////////////////////

1. UnaryRelation is removed.
                          
2. A new term ConstantNum:Term ::= \<Num:double\> is introduced for SelfCompare and SelfEq(now use Compare and Equal with the Right term as ConstantNum).

3. Term.eval() now has double as the type of return value. Therefore, Term.eval can be used by BinaryRelations for : eg. ID checking (int) or value checking (double).

4. Subsetof:BinaryRelation is introduced to cover the case if A is a subset of B. Term.Set() is used to pass String[](or maybe just pass the HashSet) to Subsetof.eval().

It is not instantiated in the Hanoi game.

5. Constraint.check(Pillar P0, Pillar P1, boolean turn) is now devide to

Constraint.oddTurn(Pillar P0, Pillar P1) and

Constraint.evenTurn(Pillar P0, Pillar P1).

6. Hanoi.present() is now named Hanoi.printResult().

7. The constraint for each move is printed as well in Constraint.oddTurn() and Constraint.EvenTurn().
